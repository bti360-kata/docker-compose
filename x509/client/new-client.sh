#!/bin/sh

CN=$1
SERIAL_NUMBER=$2

function usage() {
    echo "Usage: $0 <uid> <serial_number>"
    exit 1
}

if [ -z "${CN}" ]; then usage; fi
if [ -z "${SERIAL_NUMBER}" ]; then usage; fi

SUBJ="/CN=${CN}/OU=users/DC=demo/DC=bti360/DC=com"

echo "Generating certificate for ${SUBJ}"
openssl req -out ${CN}.csr \
            -keyout ${CN}.key \
            -new \
            -newkey rsa:2048 \
            -nodes \
            -subj ${SUBJ} || exit 1

echo "Signing CSR. You will be prompted for the CA password."
openssl x509 -req \
             -days 365 \
             -in ${CN}.csr \
             -CA ../ca/ca.crt \
             -CAkey ../ca/ca.key \
             -set_serial ${SERIAL_NUMBER} \
             -out ${CN}.crt || exit 1

echo "Creating PKCS#12 bundle. (Password=${CN})"
openssl pkcs12 -export \
               -out ${CN}.p12 \
               -inkey ${CN}.key \
               -in ${CN}.crt \
               -certfile ../ca/ca.crt \
               -passout pass:${CN} || exit 1

echo "Removing ${CN}.[csr|crt|key]"
rm -f ${CN}.csr ${CN}.crt ${CN}.key
