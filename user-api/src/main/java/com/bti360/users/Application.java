package com.bti360.users;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;

/**
 * Application runner.
 */
@SpringBootApplication
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class Application extends WebSecurityConfigurerAdapter {
    private static Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().
                anyRequest().
                authenticated().
                and().
                addFilterBefore(new X509HeaderFilter(), X509AuthenticationFilter.class).
                x509().
                userDetailsService(userDetailsService());
    }

    @Override
    protected UserDetailsService userDetailsService() {
        return userDetailsService;
    }

    @Bean
    public LdapContextSource contextSource() {
        LdapContextSource contextSource = new LdapContextSource();
        contextSource.setUrl(System.getenv("LDAP_URL"));
        contextSource.setBase(System.getenv("LDAP_BASE_DN"));
        contextSource.setUserDn(System.getenv("LDAP_BIND_USER_DN"));
        contextSource.setPassword(System.getenv("LDAP_BIND_USER_PASSWORD"));
        contextSource.afterPropertiesSet();
        return contextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(contextSource());
    }

    @Bean
    public Client elasticsearch() {
        try {
            String esHost = System.getenv("ES_HOST");
            if (esHost == null || esHost.isEmpty()) {
                throw new IllegalStateException("ES_HOST must be defined.");
            }
            Settings settings = Settings.builder()
                    .put("cluster.name", System.getenv("ES_CLUSTER_NAME"))
                    .build();
            TransportAddress address = new InetSocketTransportAddress(InetAddress.getByName(esHost), 9300);
            return new PreBuiltTransportClient(settings).addTransportAddress(address);
        } catch (UnknownHostException uhe) {
            throw new RuntimeException(uhe);
        }
    }
}
