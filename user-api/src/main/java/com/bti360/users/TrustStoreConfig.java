package com.bti360.users;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by gshankman on 5/21/17.
 */
@Component
public class TrustStoreConfig {
    private static final Logger LOG = LoggerFactory.getLogger(TrustStoreConfig.class);

    private static final char[] TRUST_PASS = new char[] {'I','L','o','v','e','O','r','a','n','g','e','!'};

    @PostConstruct
    public void sslContextConfiguration() throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException, KeyManagementException {
        if (caCertExists()) {
            LOG.info("Configuring TrustStore");
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, getTrustManagers(), null);
            SSLContext.setDefault(sslContext);
            LOG.info("TrustStore Configuration Complete");
        } else {
            LOG.info("No CA_CERT provided. Using default TrustStore.");
        }
    }

    private TrustManager[] getTrustManagers() throws NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException {
        TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        factory.init(loadTrustStore());
        return factory.getTrustManagers();
    }

    private KeyStore loadTrustStore() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        InputStream keyIn = new FileInputStream(getCaCertFile().get());
        KeyStore trustStore = KeyStore.getInstance("pkcs12");
        trustStore.load(keyIn, TRUST_PASS);
        return trustStore;
    }

    private boolean caCertExists() {
        Optional<File> certFile = getCaCertFile();
        return certFile.isPresent() && certFile.get().isFile() && certFile.get().canRead();
    }

    private Optional<File> getCaCertFile() {
        String path = System.getenv("CA_CERT");
        if (path != null && !path.trim().isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(new File(path));
        }
    }
}
