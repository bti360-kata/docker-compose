package com.bti360.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

/**
 * API Controller for Images
 */
@RestController
public class ImageController {
    private static final Logger LOG = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    private UserService userService;


    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/images/{size}/{username}.{format}")
    public byte[] getProfilePicture(Authentication authentication,
                                    Principal principal,
                                    HttpServletResponse response,
                                    @PathVariable("size") String size,
                                    @PathVariable("username") String username,
                                    @PathVariable("format") String targetFormat) throws IOException {
        if (!(isAdmin(authentication) || isCurrentUser(username, principal))) {
            throw new AccessDeniedException("Access Denied");
        }

        ImageSize imageSize;

        try {
            imageSize = ImageSize.valueOf(size.toUpperCase());
        } catch (IllegalArgumentException iae) {
            LOG.error("Unknown image size: %s", size, iae);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return new byte[0];
        }

        response.setContentType(String.format("image/%s", targetFormat).toLowerCase());
        return userService.getProfilePicture(username, imageSize, targetFormat);
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().
                stream().
                anyMatch(a -> a.getAuthority().equalsIgnoreCase("ROLE_ADMIN"));
    }

    private boolean isCurrentUser(String username, Principal principal) {
        return principal.getName().equalsIgnoreCase(username);
    }
}
