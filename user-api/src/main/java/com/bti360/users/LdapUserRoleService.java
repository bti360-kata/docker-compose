package com.bti360.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.naming.directory.DirContext;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Component
public class LdapUserRoleService implements UserRoleService, UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(LdapUserRoleService.class);

    private static final String DEFAULT_GROUP_BASE = "ou=groups";
    private static final String DEFAULT_USER_BASE = "ou=users";
    private static final String DEFAULT_USER_ID_ATTRIBUTE = "uid";

    @Autowired
    private LdapTemplate ldapTemplate;

    @Override
    public Set<UserRole> getRolesForUser(String username) {
        return new LdapUser(username).getUserRoles();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new LdapUser(username).toUserDetails();
    }

    private String getGroupBase() {
        return getEnvOrDefault("LDAP_GROUP_BASE", DEFAULT_GROUP_BASE);
    }

    private String getUserBase() {
        return getEnvOrDefault("LDAP_USER_BASE", DEFAULT_USER_BASE);
    }

    private String getUserIdAttribute() {
        return getEnvOrDefault("LDAP_USER_ID_ATTRIBUTE", DEFAULT_USER_ID_ATTRIBUTE);
    }

    private String getEnvOrDefault(String envKey, String defaultValue) {
        String envValue = System.getenv(envKey);
        return envValue != null ? envValue : defaultValue;
    }

    private class LdapUser {
        private final String username;
        private final String userDn;

        public LdapUser(String username) {
            this.username = username;
            this.userDn = loadUserDn();
            LOG.debug("Username: {}, DN: {}", this.username, this.userDn);
        }

        public Set<UserRole> getUserRoles() {
            return getGroups().
                    stream().
                    map(cn -> {
                        String roleName = cn.toUpperCase();
                        if (roleName.endsWith("S")) {
                            roleName = roleName.substring(0, roleName.length()-1);
                        }
                        try {
                            return UserRole.valueOf(roleName);
                        } catch (IllegalArgumentException|NullPointerException ex) {
                            return null;
                        }
                    }).
                    filter(Objects::nonNull).
                    collect(Collectors.toSet());
        }

        public UserDetails toUserDetails() {
            return new User(username, "", getGrantedAuthorities());
        }

        private Collection<? extends GrantedAuthority> getGrantedAuthorities() {
            return getUserRoles().
                    stream().
                    map(role -> new SimpleGrantedAuthority(String.format("ROLE_%s", role.name()))).
                    collect(Collectors.toSet());
        }

        private String loadUserDn() {
            LdapQuery userQuery = query().
                    base(getUserBase()).
                    where(getUserIdAttribute()).is(username);
            LOG.debug("Searching for user: {}", userQuery.filter().encode());
            List<String> users = ldapTemplate.search(userQuery, (ContextMapper<String>) ctx -> ((DirContext)ctx).getNameInNamespace());
            if (users.size() > 1) {
                throw new IncorrectResultSizeDataAccessException(1, users.size());
            }
            return users.isEmpty() ? null : users.get(0);
        }

        private List<String> getGroups() {
            return Stream.of(getGroupQuery("groupOfNames", "member"),
                    getGroupQuery("groupOfUniqueNames", "uniqueMember")).
                    peek(query -> LOG.debug("Executing LDAP query: {}", query.filter().encode())).
                    flatMap(query -> {
                        List<String> groups = ldapTemplate.search(query,
                                (AttributesMapper<String>) attrs -> attrs.get("cn").get().toString());
                        LOG.debug("Found groups: {}", groups);
                        return groups.stream();
                    }).
                    collect(Collectors.toList());
        }

        private LdapQuery getGroupQuery(String objectClass, String memberAttribute) {
            return query().
                    base(getGroupBase()).
                    attributes("cn").
                    where("objectClass").is(objectClass).
                    and(memberAttribute).is(userDn);
        }
    }
}
