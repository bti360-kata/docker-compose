package com.bti360.users;

public enum ImageSize {
    THUMBNAIL,
    MEDIUM,
    LARGE
}
