import { AppState } from '../';

export const getMeState = (state: AppState) => state.me;
