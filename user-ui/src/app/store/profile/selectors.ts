import { AppState } from '../';

export const getProfileState = (state: AppState) => state.profile;
