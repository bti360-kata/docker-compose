import { Action } from '@ngrx/store';

import { User } from 'app/store/models';

export const LOAD_PROFILE = '[Profile] Load Profile';
export const LOAD_PROFILE_ERROR = '[Profile] Load Profile Error';
export const LOAD_PROFILE_SUCCESS = '[Profile] Load Profile Success';

export class LoadProfileAction implements Action {
  readonly type = LOAD_PROFILE;
  constructor(public payload: string) { }
}

export class LoadProfileErrorAction implements Action {
  readonly type = LOAD_PROFILE_ERROR;
  readonly payload = {};
}

export class LoadProfileSuccessAction implements Action {
  readonly type = LOAD_PROFILE_SUCCESS;
  constructor(public payload: User) { }
}
