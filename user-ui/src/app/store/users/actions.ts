import { Action } from '@ngrx/store';

import { User } from './model';

export const LOAD_USERS = '[User] Load Users';
export const LOAD_USERS_ERROR = '[User] Load Users Error';
export const LOAD_USERS_SUCCESS = '[User] Load Users Success';

export class LoadUsersAction implements Action {
  readonly type = LOAD_USERS;
  readonly payload = {};
};

export class LoadUsersErrorAction implements Action {
  readonly type = LOAD_USERS_ERROR;
  readonly payload = {};
};

export class LoadUsersSuccessAction implements Action {
  readonly type = LOAD_USERS_SUCCESS;
  constructor(public payload: User[]) { }
};
