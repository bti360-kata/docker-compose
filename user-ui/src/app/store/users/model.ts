export type UsersState = User[];

export const initialUsersState: UsersState = [];

export interface User {
  address: string;
  gender: string;
  city: string;
  dob: Date;
  postcode: string;
  last_name: string;
  title: string;
  cell_phone: string;
  registration_date: Date;
  full_name: string;
  nationality: string;
  phone: string;
  street: string;
  id_type: string;
  state: string;
  first_name: string;
  id_value: string;
  email: string;
  username: string;
  roles: Role[];
  type: string;
}

export interface Role {
  role: string;
}

export interface Picture {
  large: String;
  medium: String;
  thumbnail: String;
}
