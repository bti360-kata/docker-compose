# Demo 2

Let's start our service back up and add the sample data.

```bash
docker-compose -f docker-compose.2.yml up -d
curl -XPOST localhost:9200/users/User/1 -d '{"name": "Gordon", "likes": "Docker"}'
curl -s localhost:9200/users/User/1 | jq
```

Now, we'll update our compose file to run on port 19200.

**_Stop Elasticsearch container. Change to host port `19200`_**

And, when we restart the service...

```bash
docker-compose -f docker-compose.2.yml stop
docker-compose -f docker-compose.2.yml up -d
curl -s localhost:19200/users/User/1 | jq
```

The data is still there!

```bash
./.d2c
```

**_PRESENTATION_**

Now that we have Elasticsearch up and running, let's add our UI and API services to the build, as well as an
OpenLDAP server to simulate the enterprise directory.
