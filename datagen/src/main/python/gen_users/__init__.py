from ._cli import cli

__versionstr__ = '${version}'
VERSION = tuple(__versionstr__.split('.'))
__version__ = VERSION
