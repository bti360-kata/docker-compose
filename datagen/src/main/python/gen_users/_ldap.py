import ldap
import ldap.modlist
import logging

_log = logging.getLogger('datagen')

_MEMBER_ATTR = 'uniquemember'


class LdapHelper(object):
    def __init__(self, ldap_url, bind_user, bind_password, user_base_dn, admin_group_dn, user_group_dn):
        """
        :param ldap_url: the LDAP URL
        :type ldap_url: str
        :param bind_user: the bind user DN
        :type bind_user: str
        :param bind_password: the bind user password
        :type bind_password: str
        :param user_base_dn: the base DN for user accounts
        :type user_base_dn: str
        :param admin_group_dn: the DN for the admin group
        :type admin_group_dn: str
        :param user_group_dn: the DN for the user group
        :type user_group_dn: str
        """
        super(LdapHelper, self).__init__()

        self._user_base_dn = user_base_dn
        self._admin_group_dn = admin_group_dn
        self._user_group_dn = user_group_dn

        self._ldap = ldap.initialize(ldap_url)
        self._ldap.simple_bind(who=bind_user, cred=bind_password)

    def add_users(self, users):
        """
        Adds the users to the directory.

        :param users: the users
        :type users: list(RandomUser)
        """
        self._clean_users(users)

        admin_dns = list()
        user_dns = [self._user_dn(x) for x in users]
        _log.info("Adding %d users to the directory", len(users))
        for user in users:
            self._add_user(user)
            if user.is_admin:
                admin_dns.append(self._user_dn(user))

        _log.info("Adding %d users to the admin group", len(admin_dns))
        if admin_dns:
            self._ldap.modify_s(self._admin_group_dn, [(ldap.MOD_ADD, _MEMBER_ATTR, admin_dns)])
        _log.info("Adding %d users to the users group", len(user_dns))
        self._ldap.modify_s(self._user_group_dn, [(ldap.MOD_ADD, _MEMBER_ATTR, user_dns)])

    def _clean_users(self, users):
        """
        Cleans the target users from the directory.

        :param users: the users to clean
        :type users: list(RandomUser)
        """
        _log.debug("Deleting users from directory.")
        delete_dns = [self._user_dn(x) for x in users]
        for dn in delete_dns:
            _log.debug("Deleting user: %s", dn)
            try:
                self._ldap.delete_s(dn)
            except ldap.NO_SUCH_OBJECT:
                pass

        _log.debug("Deleting users from admin and user groups.")
        for dn in delete_dns:
            for group in [self._admin_group_dn, self._user_group_dn]:
                try:
                    self._ldap.modify_s(group, [(ldap.MOD_DELETE, _MEMBER_ATTR, dn)])
                except ldap.NO_SUCH_ATTRIBUTE:
                    pass

    def _add_user(self, user):
        """
        Adds a user to the directory.

        :param user: the user to add
        :type user: RandomUser
        """
        attrs = {k: v.encode('utf-8') for k, v, in dict(cn=user.uid,
                                                        title=user.title,
                                                        sn=user.surname,
                                                        gn=user.given_name,
                                                        displayName=user.name,
                                                        uid=user.uid).iteritems()}
        attrs.update(dict(objectClass=['inetOrgPerson', 'organizationalPerson', 'person', 'top']))
        mod_list = ldap.modlist.addModlist(attrs)

        dn = self._user_dn(user)
        _log.debug("Adding User [%s]: %s", dn, mod_list)
        self._ldap.add_s(self._user_dn(user), mod_list)

    def _user_dn(self, user):
        """
        :param user: the user
        :type user: RandomUser

        :return: the DN for the user
        :rtype: str
        """
        return str('uid=%s,%s' % (user.uid, self._user_base_dn))
